#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Simple Bot to print/download all incoming passport data
# This program is dedicated to the public domain under the CC0 license.
"""
See https://telegram.org/blog/passport for info about what telegram passport is.

See https://git.io/fAvYd for how to use Telegram Passport properly with python-telegram-bot.

"""
import logging

from telegram.ext import Updater, MessageHandler, Filters, CommandHandler

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
					level=logging.DEBUG)

logger = logging.getLogger(__name__)

coupons = []

def novo(bot, update, args):
	bot.send_message(chat_id=update.message.chat_id, text='Digite /add Nome_do_app codigo_do_cupom validade "desconto" "regras"\nExemplo:\n/add rappi massima250 25/10 "500 em frete" "apenas novos usuarios"')

def add(bot,update,args):
	print 'agsssss',len(args)
	print args
	if len(args) != 6:
		bot.send_message(chat_id=update.message.chat_id, text='Você digitou o comando errado. Veja o exemplo:\n/add rappi massima250 25/10 "500 em frete" "apenas novos usuarios"')		
	else:
		coupon_dict = {'app':args[0],
						'codigo':args[1],
						'validade':args[2],
						'desconto':args[4],
						'regras':args[5]}
		coupons.append(coupon_dict)
		bot.send_message(chat_id=update.message.chat_id, text='Cupom %s adicionado com sucesso.' % args[1])	

def list_coupons(bot,update):	
	text = ''
	for coupon in coupons:
		text += coupon['codigo']

	bot.send_message(chat_id=update.message.chat_id, text=text)
def error(bot, update, error):
	"""Log Errors caused by Updates."""
	logger.warning('Update "%s" caused error "%s"', update, error)


def main():
	updater = Updater("592200663:AAHlbbmUVlQGbV8Dgo3kXazxUsQUGVdRzf4")
	dp = updater.dispatcher
	dp.add_handler(CommandHandler('novo', novo, pass_args=True))
	dp.add_handler(CommandHandler('add', add, pass_args=True))
	dp.add_handler(CommandHandler('list', list_coupons))


	dp.add_error_handler(error)
	updater.start_polling()
	updater.idle()


if __name__ == '__main__':
	main()